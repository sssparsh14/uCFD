# Computational Fluid Dynamics (CFD)
# Course created for undergraduate module on Computational Fluid Dynamics in Chemical Engineering at IISER Bhopal

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/sssparsh14/uCFD/master)


![Website](https://img.shields.io/website/https/github.com)


   + [**IISER Bhopal**](https://iiserb.ac.in/), Fall 2020
   + [**Dept. of Chemical Engineering**](https://che.iiserb.ac.in/index.php) (Chemical Engineering)
   + [**Dr.-Ing. Sparsh Sharma**](https://sparsh-sharma.github.io/) (sparsh.sharma@b-tu.de)

![](jet.mp4)

### Description
Computational fluid dynamics (CFD) is a branch of fluid mechanics that uses numerical analysis and data structures to analyze and solve problems that involve fluid flows. Computers are used to perform the calculations required to simulate the free-stream flow of the fluid, and the interaction of the fluid (liquids and gases) with surfaces defined by boundary conditions. With high-speed supercomputers, better solutions can be achieved, and are often required to solve the largest and most complex problems. Ongoing research yields software that improves the accuracy and speed of complex simulation scenarios such as transonic or turbulent flows. Initial validation of such software is typically performed using experimental apparatus such as wind tunnels. In addition, previously performed analytical or empirical analysis of a particular problem can be used for comparison. A final validation is often performed using full-scale testing, such as flight tests.

CFD is applied to a wide range of research and engineering problems in many fields of study and industries, including aerodynamics and aerospace analysis, weather simulation, natural science and environmental engineering, industrial system design and analysis, biological engineering, fluid flows and heat transfer, and engine and combustion analysis.


### Learning Outcomes
1) Conservation equations for mass, momentum, energy and chemical species; turbulence closure models; heat and mass transfer models; Wellposedness and boundary conditions.
2) Computations fluid dynamics concepts: discretisation, accuracy, consistency, stability and convergence; Lax’s equivalence theorem; analysis for consistency; analysis for stability; template for the solution of a scalar transport equation
3) Solution of Navier-Stokes equations: methods for compressible flow; need for special methods for incompressible flows; artificial compressibility method; stream function-vorticity method; pressure equation method; the pressure correction approach
4) Solution of discretized equations: direct methods; classical iterative methods; advanced iterative methods
5) Grid generation: structure grid generation; unstructured grid generation

### Text book
1) RH Pletcher, JC Tannehill, DA Anderson (2012) ” Computational fluid mechanics and heat transfer”, Third edition, CRC Press.
2) C. Hirsch (2007) “Numerical computation of internal and external flows: The fundamentals of computational fluid dynamics”, 2nd edition, Butterworth-Heinemann.
3) HK Versteeg, W. Malalasekera (2007) “An introduction to computational fluid dynamics: The finite volume method”, Pearson Education.


Thanks in advance for inputs to improve this course.\
Regards,\
Sparsh Sharma, PhD

